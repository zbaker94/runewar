﻿//-----------------------------------------
//          PowerSprite Animator
//  Copyright © 2016 Powerhoof Pty Ltd
//			  powerhoof.com
//----------------------------------------

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PowerTools
{

/// Component allowing animations to be played without adding them to a unity animation controller first. 
// A shared animation controller is used, it has a single state which is overridden whenever an animation is played.
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
[DisallowMultipleComponent]
public class SpriteAnim : SpriteAnimEventHandler 
{	
	#region Definitions

	static readonly int STATE_HASH = "a".GetHashCode();
	static readonly string CONTROLLER_PATH = "SpriteAnimController";

	#endregion
	#region Vars: Editor

	[SerializeField] AnimationClip m_defaultAnim = null;

	#endregion
	#region Vars: Private

	static RuntimeAnimatorController m_sharedAnimatorController = null;

	Animator m_animator = null;
	AnimatorOverrideController m_controller = null;
	AnimationClipPair[] m_clipPairArray = null;

	AnimationClip m_currAnim = null;


	#endregion
	#region Funcs: Public 

	/// Returns the currently playing clip
	public AnimationClip GetCurrentAnimation()
	{ 
		return m_currAnim; 
	}

	/// Returns the time of the currently playing clip (or zero if no clip is playing)
	public float GetCurrentAnimTime()
	{ 
		if ( m_currAnim != null )
			return m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime * m_currAnim.length;
		return 0;
	}

	///  Returns true if the passed clip is playing. If no clip is passed, returns true if ANY clip is playing
	public bool IsPlaying(AnimationClip clip = null) 
	{		
		if ( clip == null || m_currAnim == clip )
			return m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f;
		return false;
	} 

	/// Returns true if a clip with the specified name is playing
	public bool IsPlaying(string animName) 
	{ 
		if ( m_currAnim == null )
			return false;
		if ( m_currAnim.name == animName )
			return m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f;
		return false;
	} 

	/// Plays the specified clip
	public void Play(AnimationClip anim) 
	{
		if ( anim == null )
			return;

		if ( m_animator.enabled == false )
			m_animator.enabled = true;
		
		m_clipPairArray[0].overrideClip = anim;
		m_controller.clips = m_clipPairArray;

		m_animator.Play(STATE_HASH);
		m_currAnim = anim;
	}

	// Stops the clip by disabling the animator
	public void Stop()
	{		
		m_animator.enabled = false;
	}

	#endregion
	#region Funcs: Init

	void Awake()
	{
		m_controller = new AnimatorOverrideController();

		if ( m_sharedAnimatorController == null )
		{
			// Lazy load the shared animator controller
			m_sharedAnimatorController = Resources.Load<RuntimeAnimatorController>(CONTROLLER_PATH);
		}

		m_controller.runtimeAnimatorController = m_sharedAnimatorController;
		m_animator = GetComponent<Animator>();
		m_animator.runtimeAnimatorController = m_controller;
		m_clipPairArray = m_controller.clips;
		Play(m_defaultAnim);
	}

	#endregion

}

}