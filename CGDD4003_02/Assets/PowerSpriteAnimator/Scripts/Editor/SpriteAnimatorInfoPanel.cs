﻿//-----------------------------------------
//          PowerSprite Animator
//  Copyright © 2016 Powerhoof Pty Ltd
//			  powerhoof.com
//----------------------------------------

using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PowerTools
{

public partial class SpriteAnimator
{
	#region Definitions


	#endregion
	#region Vars: Private

	//bool m_preserveTimingOnFramerateChange = true;
	ReorderableList m_framesReorderableList = null;

	#endregion
	#region Funcs: Init

	void InitialiseFramesReorderableList()
	{
		m_framesReorderableList = new ReorderableList( m_frames, typeof(AnimFrame),true,true,true,true);
		m_framesReorderableList.drawHeaderCallback = (Rect rect) => 
		{ 
			EditorGUI.LabelField(rect,"Frames"); 
			EditorGUI.LabelField(new Rect(rect){x=rect.width-37,width=45},"Length"); 
		};
		m_framesReorderableList.drawElementCallback = LayoutFrameListFrame;
		m_framesReorderableList.onSelectCallback = (ReorderableList list) => 
		{ 
			ClearSelection();
			m_selectedFrames.Add(m_frames[m_framesReorderableList.index]);
		};
	}


	#endregion
	#region Funcs: Layout

	Vector2 m_scrollPosition = Vector2.zero;

	void LayoutInfoPanel( Rect rect )
	{

		GUILayout.BeginArea(rect, EditorStyles.inspectorFullWidthMargins);
		GUILayout.Space(20);

		// Animation length
		EditorGUILayout.LabelField( string.Format("Length: {0:0.00} sec  {1:D} samples", m_clip.length, Mathf.RoundToInt(m_clip.length/GetMinFrameTime())), new GUIStyle(EditorStyles.miniLabel){normal = { textColor = Color.gray }});

		// Speed/Framerate
        GUI.SetNextControlName("Framerate");
		float newFramerate = EditorGUILayout.DelayedFloatField( "Sample Rate", m_clip.frameRate );

		if ( newFramerate != m_clip.frameRate )
		{
			ChangeFrameRate(newFramerate, true);
		}

		// Looping tickbox
		bool looping = EditorGUILayout.Toggle( "Looping", m_clip.isLooping );
		if ( looping != m_clip.isLooping )
		{
			ChangeLooping(looping);
		}

		GUILayout.Space(10);

		// Frames list
		m_scrollPosition = EditorGUILayout.BeginScrollView(m_scrollPosition,false,false);
		EditorGUI.BeginChangeCheck();
		m_framesReorderableList.DoLayoutList();
		if ( EditorGUI.EndChangeCheck() )
		{
			RecalcFrameTimes();
			Repaint();
			ApplyChanges();
		}
		EditorGUILayout.EndScrollView();

		GUILayout.EndArea();

	}

	void LayoutFrameListFrame(Rect rect, int index, bool isActive, bool isFocused )
	{
		if ( m_frames == null || index < 0 || index >= m_frames.Count )
			return;
		AnimFrame frame = m_frames[index];

		EditorGUI.BeginChangeCheck();
		rect = new Rect(rect) { height = rect.height-4, y = rect.y+2 };


		// frame ID
		float xOffset = rect.x;
		float width = Styles.INFOPANEL_LABEL_RIGHTALIGN.CalcSize(new GUIContent(index.ToString())).x;
		EditorGUI.LabelField(new Rect(rect){x=xOffset,width=width},index.ToString(), Styles.INFOPANEL_LABEL_RIGHTALIGN );

		// Frame Sprite
		xOffset += width+5;
		width = (rect.xMax-5-28)-xOffset;

		// Sprite thingy
		Rect spriteFieldRect = new Rect(rect){x=xOffset,width=width,height=16};
		Texture2D oldTex = frame.m_sprite != null ? frame.m_sprite.texture : null;
		Texture2D tex = EditorGUI.ObjectField(spriteFieldRect, oldTex, typeof(Texture2D), false ) as Texture2D;	
		if ( tex != oldTex )
		{			
			Object[] assets = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(tex));
			Object subAsset = System.Array.Find(assets, item=>item is Sprite);
			if ( subAsset != null )
			{
				frame.m_sprite = (Sprite)subAsset;
			}
		}

		// Frame length (in samples)
		xOffset += width+5;
		width = 28;
		GUI.SetNextControlName("FrameLen");
		int frameLen = Mathf.RoundToInt( frame.m_length / GetMinFrameTime() );
		frameLen = EditorGUI.IntField( new Rect(rect){x=xOffset,width=width}, frameLen );
		SetFrameLength(frame, frameLen * GetMinFrameTime() );


		if ( EditorGUI.EndChangeCheck() )
		{
			// Apply events
			ApplyChanges();
		}
	}

	#endregion
	#region Funcs: Private

	void ChangeFrameRate(float newFramerate, bool preserveTiming )
	{
		Undo.RecordObject(m_clip, "Change Animation Framerate");

		// Scale each frame (if preserving timing) and clamp to closest sample time
		float minFrameTime = 1.0f/newFramerate;
		float scale = preserveTiming ? 1.0f : (newFramerate / m_clip.frameRate);
		foreach ( AnimFrame frame in m_frames )
		{
			frame.m_length = Mathf.Max( Snap( frame.m_length * scale, minFrameTime ), minFrameTime );
		}
		
		m_clip.frameRate = newFramerate;
		RecalcFrameTimes();
		ApplyChanges();
	}

	void ChangeLooping(bool looping)
	{
		Undo.RecordObject(m_clip, "Change Animation Looping");
		AnimationClipSettings settings = AnimationUtility.GetAnimationClipSettings(m_clip);
		settings.loopTime = looping;
		AnimationUtility.SetAnimationClipSettings( m_clip, settings );

		// NB: When hitting play directly after this change, the looping state will be undone. So have to call ApplyChanges() afterwards even though frame data hasn't changed.
		ApplyChanges();
	}

	#endregion
}

}