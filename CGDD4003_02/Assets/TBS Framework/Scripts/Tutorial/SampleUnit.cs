﻿using UnityEngine;

public class SampleUnit : Unit
{
    public Color LeadingColor;

    public GameObject Unit;
    public GameObject Child;

    static float tileOffsetV = 0.5f;
    static float tileOffsetH = 0.1f;

    Vector3 translation = new Vector3(0,0.3f,.5f);

    private float rotation;
    public float lerpRotate;
    public int direction;
    private float rotateSpeed;

    public override void Initialize()
    {
        base.Initialize();
        transform.position += new Vector3(0, 0, -1);
        GetComponent<Renderer>().material.color = LeadingColor;
    }

    void Awake()
    {
        GetComponent<MeshRenderer>().enabled = false;
    }

    void Start()
    {
        Child = Instantiate(Unit, new Vector3(transform.position.x , transform.position.y - tileOffsetV, transform.position.z), Quaternion.identity) as GameObject;
        Child.transform.parent = gameObject.transform;
        Child.transform.Translate(translation);

        direction = 0;
        rotation = 0;
        lerpRotate = 0;
        rotateSpeed = 1;

        Child.transform.Rotate(-70, 0, 0);
       
    }

    void Update()
    {
        GetInput();
        Rotate();
    }

    public override void MarkAsAttacking(Unit other)
    {      
    }

    public override void MarkAsDefending(Unit other)
    {       
    }

    public override void MarkAsDestroyed()
    {      
    }

    public override void MarkAsFinished()
    {
    }

    public override void MarkAsFriendly()
    {
        
    }

    public override void MarkAsReachableEnemy()
    {
       
    }

    public override void MarkAsSelected()
    {
       
    }

    public override void UnMark()
    {
       
    }

    private void GetInput()
    {
        //Rotate
        if (Input.GetKeyDown(KeyCode.E))
        {
            direction += 1;
            lerpRotate = 0;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            direction -= 1;
            lerpRotate = 0;
        }

        if (direction < 0)
            direction += 4;
        else if (direction > 3)
            direction -= 4;

        if (direction == 2 || direction == 3)
            Child.GetComponent<SpriteRenderer>().flipX = true;
        else
            Child.GetComponent<SpriteRenderer>().flipX = false;
    }

    private void Rotate()
    {
        lerpRotate = Mathf.Clamp(lerpRotate + rotateSpeed * Time.deltaTime, 0, 1);
        rotation = Mathf.LerpAngle(rotation, direction * -90, lerpRotate);
        transform.eulerAngles = new Vector3(0, 0, rotation);
    }
}
