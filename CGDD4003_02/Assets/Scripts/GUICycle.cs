﻿using UnityEngine;
using System.Collections;

public class GUICycle : MonoBehaviour {

    public Vector3 basePosition;
    public float openPositionRadians = 0;
    public bool active;
    public float radius;

    private float newT;
    private float t;
    private float speed, lerpSpeed;

    private Transform pos;

	void Start () {
        
        
        pos = GetComponent<Transform>();
        speed = 10;
        lerpSpeed = 20;
        t = 0;
        newT = 0;
	}
	
	
	void Update () {
        if (active)
        {
            newT += speed * Time.deltaTime;
        }
        else
        {
            newT -= speed * Time.deltaTime;
        }
        newT = Mathf.Clamp(newT, 0, 1);

        InterpolateCircle();
	}

    private void InterpolateCircle()
    {
        t = Mathf.Lerp(t, newT, Time.deltaTime * lerpSpeed);

        pos.localPosition = new Vector3(radius * Mathf.Cos(t * openPositionRadians), radius * Mathf.Sin(t * openPositionRadians), 0) + basePosition;
    }
}
