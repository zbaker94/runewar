﻿using UnityEngine;
using System.Collections;

public class NeedleControl : MonoBehaviour {

    public Transform camera;
    private Transform needle;

    void Start()
    {
        needle = GetComponent<Transform>();
    }
	
	void Update () {
        needle.eulerAngles = new Vector3( -camera.eulerAngles.x, 0, -camera.eulerAngles.z);
	}
}
