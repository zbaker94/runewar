﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HandGroup : MonoBehaviour {

    public int HandSize;

    public Deck deck;
    public Camera UICam, MainCam;

    public Transform UnitParent;
    public CustomUnitGenerator UnitSpawner;
    public CellGrid Board;

    public bool CardSelected;
    private Vector3 activeDisplacement, basePosition;
    private float t;
    private Transform trans;
    public float ZoomSpeed;
    public List<Card> cards;
    private Card ActiveCard;
    public float Ytrigger, Xtrigger;

    private float drawCooldown, drawTimer;

    /// <summary>
    /// Draw one card from the Deck
    /// </summary>
    void DrawFromDeck()
    {
        Card newCard = deck.DrawCard();
        if(newCard != null)
        {
            AddCard(newCard);
            newCard.Draw();
        }

    }

    /// <summary>
    /// Spawns the unit for the selected Card at a given position. Returns true if successful, false if not.
    /// </summary>
    /// <param name="UnitPos"></param>
    /// <returns></returns>
    bool SpawnUnit(Vector3 UnitPos)
    {
        GameObject go = Instantiate(ActiveCard.CardUnit, UnitParent) as GameObject;
        go.GetComponent<Transform>().position = UnitPos;
        //Set units spaces to free
        for(int i = 0; i < UnitParent.childCount - 1; i ++)
        {
            Unit u = UnitParent.GetChild(i).GetComponent<Unit>();
            if(u != null)
            {
                u.Cell.IsTaken = false;
            }
        }


        List<Unit> allUnits = UnitSpawner.SpawnUnits(Board.Cells);
        Unit unit = go.GetComponent<Unit>();
        Board.SetupUnit(unit);

        return allUnits.Contains(unit);
    }

	void Start () {
        cards = new List<Card>();
        activeDisplacement = new Vector3(0, 0.5f, -0.5f);
        trans = GetComponent<Transform>();
        CardSelected = false;
        t = 0;
        drawCooldown = 0.5f;
        drawTimer = drawCooldown;

    }
	
	
	void Update () {
        if(cards.Count < HandSize && drawTimer <= 0)
        {
            DrawFromDeck();
            drawTimer = drawCooldown;
        }

        if (drawTimer > 0)
            drawTimer -= Time.deltaTime;

        if (Input.GetMouseButtonDown(0) && CardSelected)
        {
            Ray cast = MainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(cast, out hit, 21f))
            {

                Debug.Log("Spawn");

                bool success = SpawnUnit(hit.transform.position);
                if (success)
                {
                    cards.Remove(ActiveCard);
                    GameObject.Destroy(ActiveCard.gameObject);

                    ActiveCard = null;
                    CardSelected = false;
                    UpdateCardVectors();
                }

            }
        }

        float MouseX = Input.mousePosition.x / Display.main.renderingWidth;
        float MouseY = Input.mousePosition.y / Display.main.renderingHeight;
        if ((MouseY < Ytrigger && 0.5f - Xtrigger < MouseX && MouseX < 0.5f + Xtrigger) )
            t = Mathf.Clamp(t + Time.deltaTime * ZoomSpeed, 0, 1);
        else
            t = Mathf.Clamp(t - Time.deltaTime * ZoomSpeed, 0, 1);

        if(!CardSelected)
            CheckCards();

        basePosition = trans.position + t * activeDisplacement;

        


	}

    public void AddCard(Card newCard = null)
    {
        GameObject go;
        if (newCard == null)
        {
            go = Instantiate(Resources.Load("Card")) as GameObject;
            newCard = go.GetComponent<Card>();
            Debug.Log("Added Generic card: DOES NOT DO ANYTHING");
        }
        else
        {
            go = newCard.gameObject;
        }

        go.GetComponent<Transform>().SetParent(GetComponent<Transform>());
        newCard.Hand = this;
        cards.Add(newCard);
        UpdateCardVectors();
    }

    private void UpdateCardVectors()
    {
        int n = cards.Count-1;
        for(int i = 0; i < cards.Count; i++)
        {
            cards[i].fullDisplace = new Vector3(0, 1f, -0.5f);
            float numDeltas = (i - (0.5f * n));
            float delta = Mathf.Clamp((3f / n), -0.8f, 0.8f);
            
            cards[i].minDisplace = new Vector3( numDeltas * delta, 0, 0.01f * i);
        }
    }

    public Vector3 GetHandPos()
    {
        return basePosition;
    }

    
    private void CheckCards()
    {
        Ray MouseRay = UICam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(MouseRay, out hit, 10f))
        {
            foreach(Card c in cards)
            {
                if (hit.transform == c.transform)
                {
                    c.Hovered = true;
                    ActiveCard = c;
                }
                else
                {
                    c.Hovered = false;
                }
            }
        }
        else if (ActiveCard != null)
        {
            ActiveCard.Hovered = false;
            ActiveCard = null;
        }
    }

}
