﻿using UnityEngine;
using System.Collections;

public class GUICysleControl : MonoBehaviour {

    public float[] mouseRangeX, mouseRangeY;
    public GUICycle[] Components;

	// Update is called once per frame
	void Update () {
        float x = Input.mousePosition.x / Screen.width;
        float y = Input.mousePosition.y / Screen.height;
        
        if (x > mouseRangeX[0] && x < mouseRangeX[1] && y > mouseRangeY[0] && y < mouseRangeY[1])
        {
            foreach(GUICycle button in Components)
            {
                button.active = true;
            }
        }
        else
        {
            foreach (GUICycle button in Components)
            {
                button.active = false;
            }
        }
	}
}
