﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour {

    private enum CardState
    {
        InDeck, InHand, Transition
    }

    public GameObject CardUnit;
    public bool Hovered;
    public bool Selected;

    
    public Vector3 fullDisplace;
    public Vector3 minDisplace;
    private float ZoomSpeed, drawSpeed;
    private float t, t2;
    public HandGroup Hand;
    
    private Transform trans;
    public Vector3 selectDisplace;

    public Vector3 deckPos;
    public static Vector3 DrawControlPoint;

    private CardState state;

	void Start () {
        ZoomSpeed = 10;
        drawSpeed = 2;
        trans = GetComponent<Transform>();
        
	}
	
    public void InDeck()
    {
        state = CardState.InDeck;
    }
    public void Draw()
    {
        state = CardState.Transition;
    }

	void LateUpdate () {

        switch (state)
        {
            case CardState.InHand:
                trans.position = Hand.GetHandPos() + minDisplace + t * fullDisplace + t2 * (selectDisplace - Hand.GetHandPos() - minDisplace);
                if (Hovered)
                    t = Mathf.Clamp(t + Time.deltaTime * ZoomSpeed, 0, 1);
                else
                    t = Mathf.Clamp(t - Time.deltaTime * ZoomSpeed, 0, 1);

                if (Selected)
                    t2 = Mathf.Clamp(t2 + Time.deltaTime * ZoomSpeed, 0, 1);
                else
                    t2 = Mathf.Clamp(t2 - Time.deltaTime * ZoomSpeed, 0, 1);

                if (Input.GetMouseButtonDown(0) && Hovered)
                {
                    Selected = !Selected;
                    Hand.CardSelected = !Hand.CardSelected;
                }
                break;
            case CardState.InDeck:

                break;
            case CardState.Transition:
                //Bezier from deck to hand
                trans.position = deckPos * (1 - 2 * t + t * t) + 2 * DrawControlPoint * (t - t * t) + Hand.GetHandPos() * t * t;

                //Interpolate Angle
                trans.eulerAngles = new Vector3(0, Mathf.LerpAngle(180, 0, t), 0);
                if (t >= 1)
                {
                    state = CardState.InHand;
                    
                }
                else
                    t += Time.deltaTime * drawSpeed;

                break;
        }

        

    }
}
