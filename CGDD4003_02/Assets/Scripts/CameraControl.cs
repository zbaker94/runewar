﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    public Transform CamHolder;
    private Transform cam;
    private string AxisVertical, AxisHorizontal, AxisZoom;

    //Camera Lock Parameters
    private float maxDisplaceH, maxDisplaceV, defaultZ;

    //For Bezier Zoom
    private float t, newT;
    private Vector3 p1, p2, p3;

    //For Motion
    private float X;
    private float Y;
    private float transformSpeed;
    private float zoomSpeed;
    private float zoomSensitivity;
    private float lerpSpeed;

    //For Angle
    private float downAngle;
    private float forwardAngle;
    private float transitionPoint;

    //For Rotation
    private float rotation;
    public float lerpRotate;
    public int direction;
    private float rotateSpeed;

	void Start () {
        t = 0;
        newT = t;
        cam = GetComponent<Transform>();
        p1 = new Vector3(0, 0, 0);
        p2 = new Vector3(0, -3f, 5);
        p3 = new Vector3(0, 0.25f, 19.5f);

        maxDisplaceH = 10;
        maxDisplaceV = 10;
        defaultZ = -20;

        AxisHorizontal = "Horizontal";
        AxisVertical = "Vertical";
        AxisZoom = "Mouse ScrollWheel";

        X = 12;
        Y = 7;
        transformSpeed = 40f;
        zoomSpeed = 10f;
        zoomSensitivity = .5f;

        downAngle = 0;
        forwardAngle = -90;
        transitionPoint = 0.5f;
        lerpSpeed = 20;

        direction = 0;
        rotation = 0;
        lerpRotate = 0;
        rotateSpeed = 1;
	}
	
	
	void Update () {
        GetInput();

        
        SetBasePosition();
        SetZoomPosition();
        
        SetAngle();
        Rotate();
    }

    private void GetInput()
    {
        //Move along the Plane
        float xIn = Input.GetAxis(AxisHorizontal) * Time.deltaTime * transformSpeed;
        float yIn = Input.GetAxis(AxisVertical) * Time.deltaTime * transformSpeed;

        X = Mathf.Lerp(X, X + (xIn *Mathf.Cos(rotation * Mathf.PI / 180) -  yIn* Mathf.Sin(rotation * Mathf.PI / 180)), lerpSpeed * Time.deltaTime);
        Y = Mathf.Lerp(Y, Y + (xIn * Mathf.Sin(rotation * Mathf.PI / 180) + yIn * Mathf.Cos(rotation * Mathf.PI / 180)), lerpSpeed * Time.deltaTime);

        //Zoom
        newT += Input.GetAxis(AxisZoom) * zoomSensitivity;
        newT = Mathf.Clamp(newT, 0, 1);

        //Rotate
        if (Input.GetKeyDown(KeyCode.E))
        {
            direction += 1;
            lerpRotate = 0;
        }
            
        if (Input.GetKeyDown(KeyCode.Q))
        {
            direction -= 1;
            lerpRotate = 0;
        }
            
        if (direction < 0)
            direction += 4;
        else if (direction > 3)
            direction -= 4;
        
    }

    private void SetZoomPosition()
    {
        t = Mathf.Lerp(t, newT, Time.deltaTime * zoomSpeed);

        if (t < 0)
            t = 0;
        else if (t > 1)
            t = 1;

        cam.localPosition = ((t * t) - (2 * t) + 1) * p1 + 2 * (t - (t * t)) * p2 + (t * t) * p3;
    }

    private void SetBasePosition()
    {
        if (X > maxDisplaceH)
            X = maxDisplaceH;
        else if (X < -1 * maxDisplaceH)
            X = maxDisplaceH * -1;
        
        if (Y > maxDisplaceV)
            Y = maxDisplaceV;
        else if (Y < -1 * maxDisplaceV)
            Y = maxDisplaceV * -1;

        

        CamHolder.position = new Vector3(X, Y, defaultZ);
    }

    private void SetAngle()
    {
        if (t < transitionPoint)
        {
            cam.localEulerAngles = new Vector3(0, downAngle, 0);
        }
        else
        {
            cam.localEulerAngles = new Vector3(Mathf.LerpAngle(downAngle, forwardAngle, (t-transitionPoint)/(1-transitionPoint)), 0, 0);
        }
    }

    private void Rotate()
    {
        lerpRotate = Mathf.Clamp(lerpRotate + rotateSpeed * Time.deltaTime, 0, 1);
        rotation = Mathf.LerpAngle(rotation, direction * -90, lerpRotate);
        CamHolder.eulerAngles = new Vector3(0, 0, rotation);
    }

}
