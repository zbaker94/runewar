﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class UnitSummon : MonoBehaviour, IUnitGenerator {

    public Transform CellParent, UnitParent;
    private Unit UnitToSpawn;
    private Vector3 SpawnPosition;

    /// <summary>
    /// Returns true if unit is successfully spawned
    /// </summary>
    /// <param name="unit"></param>
    /// <param name="location"></param>
    /// <returns></returns>
    public bool SafeSpawnUnit(Unit unit, Vector3 location)
    {
        UnitToSpawn = unit;
        SpawnPosition = location;
        List<Cell> cells = CellParent.GetComponentsInChildren<Cell>().ToList<Cell>();
        return SpawnUnits(cells) == null;
    }

    /// <summary>
    /// DO NOT USE
    /// </summary>
    /// <param name="cells"></param>
    /// <returns></returns>
    public List<Unit> SpawnUnits(List<Cell> cells)
    {
        if (UnitToSpawn != null)
        {
            GameObject go = Instantiate(UnitToSpawn.gameObject, UnitParent, false) as GameObject;
            Unit unit = go.GetComponent<Unit>();

            var cell = cells.OrderBy(h => Math.Abs((h.transform.position - SpawnPosition).magnitude)).First();
            if (!cell.IsTaken)
            {

                cell.IsTaken = true;
                unit.Cell = cell;
                SnapUnit(unit, cell);
                unit.Initialize();
                List<Unit> ret = new List<Unit>();
                ret.Add(unit);
                return ret;
            }
            else
            {
                return null;
            }

        }
        else return null;
    }

    private void SnapUnit(Unit u, Cell c)
    {
        Vector3 offset = new Vector3(0, 0, c.GetComponent<Cell>().GetCellDimensions().z);
        u.GetComponent<Transform>().position = c.transform.position - offset;
    }
    
}
