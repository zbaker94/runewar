﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Deck : MonoBehaviour {

    private List<Card> deck;

	
	void Start () {
        deck = new List<Card>();
        Card.DrawControlPoint = new Vector3(0, 0, 0);
        foreach(Card c in GetComponentsInChildren<Card>())
        {
            deck.Add(c);
            c.deckPos = GetComponent<Transform>().position;
        }
	}
	
	public Card DrawCard()
    {
        if (deck.Count > 0)
        {
            Card topCard = deck[0];
            deck.RemoveAt(0);
            return topCard;
        }
        else return null;
    }

}
