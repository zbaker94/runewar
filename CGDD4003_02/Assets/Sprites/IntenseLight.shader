// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/IntenseLight" {
	Properties{
		//Color of the Material
		_Color("Color", Color) = (1,1,1,1)
		//Boosts the vibrancy of the color/light
		_Intensity("Intensity Boost", Range(0, 3)) = 2

		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#include "UnityCG.cginc"

			#pragma target 2.0
			#pragma vertex vertexShader
			#pragma fragment fragmentShader

			float4 _LightColor0;
			float4 _Color;
			float _Intensity;
			sampler2D _MainTex;
			

			struct vsIn {
				float4 position : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct vsOut {
				float4 position : SV_POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			vsOut vertexShader(vsIn v)
			{
				vsOut o;
				o.position = mul(UNITY_MATRIX_MVP, v.position);
				o.normal = normalize(mul(v.normal, unity_WorldToObject));
				o.uv = v.uv;
				return o;
			}

			float4 fragmentShader(vsOut psIn) : SV_Target
			{
				float4 AmbientLight = UNITY_LIGHTMODEL_AMBIENT;

				float4 LightDirection = normalize(_WorldSpaceLightPos0);

				float4 diffuseTerm = saturate(dot(LightDirection, psIn.normal));
				float4 DiffuseLight = diffuseTerm * _LightColor0;

				float4 TotalLight = AmbientLight + DiffuseLight;
				
				return tex2D(_MainTex, psIn.uv) * TotalLight * _Color * _Intensity;
			}

			ENDCG
		}
	}
}