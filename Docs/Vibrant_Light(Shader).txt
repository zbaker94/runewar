Vibrant Light Shader
-This Shader is used for a textured material with lighting provided by the scene.
-It includes an Intensity value to boost the brightness of the color/light
-This shader can be used for any item that requires its lighting to be boosted or dimmed for whatever reason, such as UI that responds to world lighting but must always remain visible.

-Henry Homilelr